package truman.testbed;

import java.io.Serializable;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletRequest;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;

public class TestVM implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -888223649615676973L;
	private List<String> payMethods;
	private List<List<CheckBean>> listOfLists = new ArrayList<List<CheckBean>>();
	private List<Item> list;
	private List<Item> bigList;
	private List<CheckBean> chkList;
	private List<CheckBean> tList;
	private String msg = "msg";
	private int selR = 0;
	private int count = 0;
	private int numColumns = 6;
	private int numRows;

	@Init
	public void init() {
		System.out.println("initializing TestVM");
		list = createList(3);
		bigList = createList(100);
		// gettList();
	}

	public List<Item> createList(int size) {
		List<Item> list = new ArrayList<Item>();
		Calendar cal = Calendar.getInstance();
		for (int i = 0; i < size; i++) {
			list.add(new Item(i, "item " + i, null, cal.getTime()));
			cal.add(Calendar.HOUR_OF_DAY, 1);

		}
		return list;
	}

	public List<Item> getList() {
		return list;
	}

	public List<Item> getBigList() {
		return bigList;
	}

	public List<String> getPayMethods() {
		if (payMethods == null) {
			payMethods = new ArrayList<String>();
			for (PAYMETHODS p : PAYMETHODS.values()) {
				System.out.println(p.getMethod());
				payMethods.add(p.getCode() + "-" + p.getMethod());
			}
		}
		return payMethods;
	}

	public void setPayMethods(List<String> l) {
		payMethods = l;
	}

	@Command
	@NotifyChange("msg")
	public void selAll() {
		/*
		 * Iterator<CheckBean> it = chkList.iterator(); String
		 * selectedPayMethods = "Selected:"; while(it.hasNext()){ CheckBean n =
		 * (CheckBean)it.next(); if(n.isChked())
		 * selectedPayMethods=selectedPayMethods
		 * .concat(n.getCode()).concat(","); } selectedPayMethods =
		 * selectedPayMethods.substring(0,selectedPayMethods.length()-1);
		 * setMsg(selectedPayMethods);children="@load(vm.tList)"
		 */

		// String [] testers = {"Check","Cash","Credit Card","Transfer","Free"};
		// List<String> testList = Arrays.asList(testers);
		// String tester="2";
		PAYMETHODS p = PAYMETHODS.valueOf("METHOD" + (selR + 1));
		setMsg(p.getMethod());

	}

	public List<CheckBean> getChkList() {
		List<List<CheckBean>> theList = new ArrayList<List<CheckBean>>();
		System.out.println("getting check list");
		if (chkList == null) {
			System.out.println("null-getting check list");
			chkList = new ArrayList<CheckBean>();
			for (PAYMETHODS p : PAYMETHODS.values()) {
				CheckBean cb = new CheckBean();
				cb.setChked(false);
				cb.setCode(p.getCode());
				cb.setName(p.getMethod());
				chkList.add(cb);
			}
		}
		return chkList;
	}

	public void setMsg(String m) {
		msg = m;
	}

	public String getMsg() {
		return msg;
	}

	public int getSelR() {
		return selR;
	}

	public void setSelR(int selR) {
		this.selR = selR;
	}

	public List<CheckBean> gettList() {
		List<CheckBean> tempList;
		if (tList == null) {
			System.out.println("getting tlist -null:");
			List<String> aList = Arrays
					.asList("A-Apple", "B-Banana", "C-Coconut", "D-Durian",
							"E-Eggplant", "F-Fig", "G-Grape", "H-Hickory",
							"I-Ice tea", "J-Juicy peach", "K-Kiwi", "L-Lemon",
							"M-Melon", "N-Nectarin", "O-Orange", "P-Pineapple",
							"Q-Quarenden", "R-Raspberry", "S-Strawberry");
			tList = new ArrayList<CheckBean>();
			for (String s : aList) {
				CheckBean bean = new CheckBean();
				bean.setCode(s.substring(0, 1));
				bean.setName(s.substring(2));
				tList.add(bean);
			}
			numRows = tList.size() % numColumns == 0 ? tList.size()
					/ numColumns : tList.size() / numColumns + 1;
			tempList = tList;
		} else {

			int index = count % numRows;
			tempList = tList.subList(index * numColumns, (index + 1)
					* numColumns > tList.size() ? tList.size() : (index + 1)
					* numColumns);
			System.out.println("getting tlist count:" + (count++));
		}

		return tempList;
	}

	public List<CheckBean> getcList() {
		if (tList == null)
			gettList();
		return tList;
	}

	public List<List<CheckBean>> getListOfLists() {
		if (listOfLists.size() == 0) {
		List<String> aList = Arrays.asList("A-Apple", "B-Banana", "C-Coconut",
				"D-Durian", "E-Eggplant", "F-Fig", "G-Grape", "H-Hickory",
				"I-Ice tea", "J-Juicy peach", "K-Kiwi", "L-Lemon", "M-Melon",
				"N-Nectarin", "O-Orange", "P-Pineapple", "Q-Quarenden",
				"R-Raspberry", "S-Strawberry");
		List<List<CheckBean>> theList = new ArrayList<List<CheckBean>>();
		
			tList = new ArrayList<CheckBean>();
			for (String s : aList) {
				CheckBean bean = new CheckBean();
				bean.setCode(s.substring(0, 1));
				bean.setName(s.substring(2));
				tList.add(bean);
			}
			numRows = tList.size() % numColumns == 0 ? tList.size()
					/ numColumns : tList.size() / numColumns + 1;
			for (int j = 0; j < numRows; j++) {
				List<CheckBean> newlist = new ArrayList<CheckBean>();
				newlist.addAll(tList.subList(j * numColumns, (j + 1)
						* numColumns > tList.size() ? tList.size() : (j + 1)
						* numColumns));
				listOfLists.add(newlist);
			}
		}
		return listOfLists;
	}

	public int getCount() {
		return count;
	}

	@Command
	@NotifyChange("checkedCodes")
	public void checkCodes() {

	}

	public String getIpaddr() {
		return Executions.getCurrent().getRemoteAddr();
	}

	public String getRhost() {
		return Executions.getCurrent().getRemoteHost();
	}

	public int getPortNum() {
		ServletRequest request;
		request = (ServletRequest) Executions.getCurrent().getNativeRequest();
		return request.getRemotePort();
	}

	public String getCheckedCodes() {
		String codes = "";
		for (CheckBean bean : tList) {
			if (bean.isChked()) {
				codes = codes.concat(bean.getCode()).concat(",");
			}
		}
		codes = "".equals(codes) ? "none" : codes.substring(0,
				codes.length() - 1);
		return codes;
	}

	public int getNumColumns() {
		return numColumns;
	}
}
