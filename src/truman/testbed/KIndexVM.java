package truman.testbed;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cyberneko.html.parsers.DOMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;

import truman.testbed.KIndexing.model.KDBean;
import truman.testbed.KIndexing.model.Stocks;

public class KIndexVM {
	/**
	 * logger
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(KIndexVM.class);
	private String selectedCode;
	private Map<String, Float> kmap=new HashMap<String, Float>();
	private Map<String, Float> dmap=new HashMap<String, Float>();
	private Stocks stocks = new Stocks();
	private List<KDBean> kdlist = new ArrayList<KDBean>();
	private double kvalue = 0.0, dvalue = 0.0;
	private Date dataDate = new Date();
	private int counter = -1;

	@SuppressWarnings("unchecked")
	@Init
	public void init() {
		String twseUrl="http://www.twse.com.tw/ch/trading/exchange/STOCK_DAY/genpage/Report%s/%s_F3_1_8_%s.php?STK_NO=%s&amp;myear=%s&amp;mmon=%s";
		stocks.setStockList(Arrays.asList("0050","1301","1303","2317","2357","2881","2885","4938"));
		stocks.setTwseUrl(twseUrl);
		if (stocks.getStockList().size() > 0) {
			selectedCode = stocks.getStockList().get(0);
		}
	}

	@Command
	@NotifyChange("kdlist")
	public void query0() {
		SimpleDateFormat formater = new SimpleDateFormat("yyyyMM");
		String ymstring = formater.format(new Date());
		stocks.setTwseUrl(String.format(stocks.getTwseUrl(), ymstring,
				ymstring, selectedCode, selectedCode, ymstring.substring(0, 4),
				ymstring.substring(4)));
		LOGGER.debug(stocks.getTwseUrl());
		System.out.println(stocks.getTwseUrl());
		kdlist.clear();
		counter = -1;
		DOMParser parser = new DOMParser();
		Document document = null;
		try {
			parser.parse(stocks.getTwseUrl());
			document = parser.getDocument();
			NodeList nlist = document.getElementsByTagName("table");
			int skip = 0;
			Node node = nlist.item(7);

			// digger0(node, new KDBean());
			NodeList tblist = node.getChildNodes();
			for (int i = 0; i < tblist.getLength(); i++) {
				System.out.println("TBList size:" + tblist.getLength());
				Node bnode = tblist.item(i);
				if (bnode.getNodeName().equals("TBODY")) {
					NodeList trlist = bnode.getChildNodes();
					for (int j = 0; j < trlist.getLength(); j++) {
						Node rnode = trlist.item(j);
						if (rnode.getNodeName().equals("TR")) {
							if (++skip < 3) {
								continue;
							}
							KDBean kdbean = new KDBean();
							NodeList tdlist = rnode.getChildNodes();
							for (int k = 0; k < tdlist.getLength(); k++) {
								Node dnode = tdlist.item(k);
								if (dnode.getNodeName().equals("TD")) {
									NodeList clist = dnode.getChildNodes();
									for (int m = 0; m < clist.getLength(); m++) {
										Node cnode = clist.item(m);
										if (cnode.getNodeName().equals("DIV")) {
											NodeList dlist = cnode
													.getChildNodes();
											for (int n = 0; n < dlist
													.getLength(); n++) {
												Node tnode = dlist.item(n);
												if (tnode != null) {
													System.out.println(tnode
															.getNodeValue());
													kdbean.setDate(tnode
															.getNodeValue());
													if (tnode
															.getNodeValue()
															.matches(
																	stocks.getKdate())) {
														kdbean.setKvalue(kmap
																.get(selectedCode));
														kdbean.setDvalue(dmap
																.get(selectedCode));
													}
												}
											}
										} else {
											System.out.println("NodeName:"
													+ cnode.getNodeName());
											if (cnode.getNodeValue() != null
													&& !cnode.getNodeValue()
															.trim().equals("")) {
												switch (k) {
												case 4:
													kdbean.setHigh(Float.valueOf(cnode
															.getNodeValue()));
													break;
												case 5:
													kdbean.setLow(Float.valueOf(cnode
															.getNodeValue()));
													break;
												case 6:
													kdbean.setClose(Float.valueOf(cnode
															.getNodeValue()));
													break;
												}
											}
											System.out.println(cnode
													.getNodeValue());
										}
									}
								}
							}
							if (kdbean.getClose() != 0.0) {
								kdlist.add(kdbean);
							}
						} else if (rnode.getNodeName().equals("TABLE")) {
							break;
						}
					}
				}
			}
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		int count = kdlist.size() - 1;
		caculate();
		predict();
		// kdlist = kdlist.subList(count, kdlist.size());
	}

	@Command
	@NotifyChange("kdlist")
	public void query() throws ParseException {
		String sDate="00000000";
		SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd");
		String temps= formater.format(dataDate);
		stocks.setKdate((Integer.parseInt(temps.substring(0,4))-1911)+temps.substring(4));
		System.out.println("kdate:-------------> "+stocks.getKdate());
		kmap.put(selectedCode, (float) kvalue);
		dmap.put(selectedCode, (float) dvalue);
		List<KDBean> templist = new ArrayList<KDBean>();
		kdlist.clear();
		parseHTMLTable(dataDate);
		Calendar cal = Calendar.getInstance(); 
		if(kdlist.size()>9){
			 sDate=kdlist.get(9).getDate();
			sDate = (Integer.valueOf(sDate.substring(0, sDate.indexOf("/")))+1911)+sDate.substring(sDate.indexOf("/"));
		}
		if (kdlist.size() < 9||dataDate.before(formater.parse(sDate))) {
			cal.setTime(dataDate);
			cal.get(Calendar.DAY_OF_MONTH);			
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1);
			templist.addAll(kdlist);
			kdlist.clear();
			parseHTMLTable(cal.getTime());
			kdlist.addAll(templist);
		}
		// int count = kdlist.size() - 1;
		caculate();
		predict();
		// kdlist = kdlist.subList(count, kdlist.size());
	}

	private void parseHTMLTable(Date theDate) {
		SimpleDateFormat formater = new SimpleDateFormat("yyyyMM");
		String ymstring = formater.format(theDate);
		String url = String.format(stocks.getTwseUrl(), ymstring, ymstring,
				selectedCode, selectedCode, ymstring.substring(0, 4),
				ymstring.substring(4));
		LOGGER.debug(stocks.getTwseUrl());
		System.out.println(stocks.getTwseUrl());
		counter = -1;
		DOMParser parser = new DOMParser();
		Document document = null;
		try {
			parser.parse(url);
			document = parser.getDocument();
			NodeList nlist = document.getElementsByTagName("table");
			readNodes(nlist.item(7), null);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void readNodesLoop(Node node) {
		while (true)
			;
	}

	private void readNodes(Node node, KDBean bean) {
		NodeList nodelist = node.getChildNodes();
		if (node.getNodeName().matches("TR") && nodelist.getLength() == 9) {
			bean = new KDBean();
		} else if (node.getNodeName().matches("#text")) {
			if (node.getParentNode().getNodeName().matches("DIV")
					&& node.getNodeValue() != null) {
				if (node.getNodeValue().indexOf("/") > 0) {
					bean.setDate(node.getNodeValue());
					if (node.getNodeValue().matches(stocks.getKdate())) {
						bean.setKvalue(kmap.get(selectedCode));
						bean.setDvalue(dmap.get(selectedCode));
					}
					counter = 1;
				}
			} else {
				switch (++counter) {
				case 5:
					bean.setHigh(Float.valueOf(node.getNodeValue()));
					break;
				case 6:
					bean.setLow(Float.valueOf(node.getNodeValue()));
					break;
				case 7:
					bean.setClose(Float.valueOf(node.getNodeValue()));
					kdlist.add(bean);
					break;
				}
			}
			return;
		}
		for (int i = 0; i < nodelist.getLength(); i++) {
			if (nodelist.item(i).getNodeName().matches("TABLE")) {
				break;
			}
			readNodes(nodelist.item(i), bean);
		}
	}

	@Command
	public void caculate() {
		float previousk = 0;
		float previousd = 0;
		boolean begin = false;
		for (KDBean bean : kdlist) {
			System.out.println(bean.getDate());
			if(!begin){
				if(bean.getDate().matches(stocks.getKdate())){
					previousk = bean.getKvalue();
					previousd = bean.getDvalue();					
					begin=true;
					continue;
				}
			}
			if (begin) {
				float maxf = max9(
						kdlist.subList(kdlist.indexOf(bean) - 8,
								kdlist.indexOf(bean) + 1), true);
				float minl = max9(
						kdlist.subList(kdlist.indexOf(bean) - 8,
								kdlist.indexOf(bean) + 1), false);
				float rsv = 100 * (bean.getClose() - minl) / (maxf - minl);
				bean.setKvalue(Float.valueOf(String.format("%.2f",
						(2 * previousk / 3 + rsv / 3))));
				bean.setDvalue(Float.valueOf(String.format("%.2f",
						(2 * previousd / 3 + bean.getKvalue() / 3))));
				previousk = bean.getKvalue();
				previousd = bean.getDvalue();
			} //else {
//				previousk = bean.getKvalue();
//				previousd = bean.getDvalue();
			//}
		}
	}

	public List<String> getStockList() {
		return stocks.getStockList();
	}

	/**
	 * @return the selectedCode
	 */
	public String getSelectedCode() {
		return selectedCode;
	}

	/**
	 * @param selectedCode
	 *            the selectedCode to set
	 */
	public void setSelectedCode(String selectedCode) {
		this.selectedCode = selectedCode;
	}

	/**
	 * @return the kdlist
	 */
	public List<KDBean> getKdlist() {
		return kdlist;
	}

	/**
	 * @return the stocks
	 */
	public Stocks getStocks() {
		return stocks;
	}

	/**
	 * @param stocks
	 *            the stocks to set
	 */
	public void setStocks(Stocks stocks) {
		this.stocks = stocks;
	}

	private float max9(List<KDBean> list, boolean horl) {
		List<Float> flist = new ArrayList<Float>();
		for (KDBean bean : list) {
			if (horl) {
				flist.add(bean.getHigh());
				// System.out.println("H:" + bean.getHigh());
			} else {
				flist.add(bean.getLow());
				// System.out.println("L:" + bean.getLow());
			}
		}
		Collections.sort(flist);
		// System.out.println("Max:" + flist.get(flist.size() - 1) + "Min:" +
		// flist.get(0));
		if (horl) {
			return flist.get(flist.size() - 1);
		} else {
			return flist.get(0);
		}
	}

	private void predict() {
		float close = kdlist.get(kdlist.size() - 1).getClose();
		float previousk = kdlist.get(kdlist.size() - 1).getKvalue();
		float previousd = kdlist.get(kdlist.size() - 1).getDvalue();
		float hight = max9(kdlist.subList(kdlist.size() - 9, kdlist.size()),
				true);
		float low = max9(kdlist.subList(kdlist.size() - 9, kdlist.size()),
				false);
		List<KDBean> plist = new ArrayList<KDBean>();
		plist.addAll(kdlist.subList(kdlist.size() - 9, kdlist.size()));
		float bound= Float.parseFloat(String.format("%.2f", close*0.07));
		float step=0.1f;
		if(bound>7.0f){
			step=0.5f;
		}
		for (float i = step; i < bound; i += step) {
			for (float j = step; j < bound; j += step) {
				KDBean bean = new KDBean();
				bean.setDate(close + "+" + String.format("%.2f", i));
				bean.setClose(Float.valueOf(String.format("%.2f", close + i)));
				bean.setHigh(Float.valueOf(String.format("%.2f",
						Math.max(close + i, hight))));
				bean.setLow(Float.valueOf(String.format("%.2f",
						Math.min(close - j, low))));
				plist.add(bean);
				float maxf = max9(plist, true);
				float minl = max9(plist, false);
				float rsv = 100 * (bean.getClose() - minl) / (maxf - minl);
				plist.remove(bean);
				bean.setKvalue(Float.valueOf(String.format("%.2f",
						(2 * previousk / 3 + rsv / 3))));
				bean.setDvalue(Float.valueOf(String.format("%.2f",
						(2 * previousd / 3 + bean.getKvalue() / 3))));
				kdlist.add(bean);
			}
		}
		for (float i = step; i < bound; i += step) {
			for (float j = step; j < bound; j += step) {
				KDBean bean = new KDBean();
				bean.setDate(close + "-" + String.format("%.2f", i));
				bean.setClose(Float.valueOf(String.format("%.2f", close - i)));
				bean.setHigh(Float.valueOf(String.format("%.2f",
						Math.max(close + j, hight))));
				bean.setLow(Float.valueOf(String.format("%.2f",
						Math.min(close - i, low))));
				plist.add(bean);
				float maxf = max9(plist, true);
				float minl = max9(plist, false);
				float rsv = 100 * (bean.getClose() - minl) / (maxf - minl);
				plist.remove(bean);
				bean.setKvalue(Float.valueOf(String.format("%.2f",
						(2 * previousk / 3 + rsv / 3))));
				bean.setDvalue(Float.valueOf(String.format("%.2f",
						(2 * previousd / 3 + bean.getKvalue() / 3))));
				kdlist.add(bean);
			}
		}
	}

	private void digger(Node node, KDBean bean) {
		// debug info
		System.out.print("+");
		for (int x = 0; x < counter; x++) {
			System.out.print("-");
		}
		System.out.print(node.getNodeName());
		NodeList nodelist = node.getChildNodes();
		System.out.println(":" + nodelist.getLength());
		// debug info
		if (node.getNodeName().matches("TR") && nodelist.getLength() == 9) {
			bean = new KDBean();
		} else if (node.getNodeName().matches("#text")) {
			if (node.getParentNode().getNodeName().matches("DIV")
					&& node.getNodeValue() != null) {
				if (node.getNodeValue().indexOf("/") > 0) {
					System.out.println(node.getNodeValue() + "<<<<<<< Date");
					bean.setDate(node.getNodeValue());
					if (node.getNodeValue().matches(stocks.getKdate())) {
						bean.setKvalue(kmap.get(selectedCode));
						bean.setDvalue(dmap.get(selectedCode));
					}
					counter = 1;
				} else if (node.getNodeValue().indexOf(".") > 0) {
					System.out.println(node.getNodeValue() + "<<<<<<< Number?");
				} else {
					System.out.println(node.getNodeValue() + "<<<<<<< ?");
				}
			} else {
				switch (++counter) {
				case 5:
					System.out.println(node.getNodeValue() + "<<<<<<< value");
					bean.setHigh(Float.valueOf(node.getNodeValue()));
					break;
				case 6:
					System.out.println(node.getNodeValue() + "<<<<<<< value");
					bean.setLow(Float.valueOf(node.getNodeValue()));
					break;
				case 7:
					System.out.println(node.getNodeValue() + "<<<<<<< value");
					bean.setClose(Float.valueOf(node.getNodeValue()));
					// KDBean beanx = new KDBean();
					// BeanUtils.copyProperties(bean, beanx);
					kdlist.add(bean);
					break;
				default:
					System.out.println("--" + node.getNodeValue());
				}
			}
			return;
		}

		for (int i = 0; i < nodelist.getLength(); i++) {
			if (nodelist.item(i).getNodeName().matches("TABLE")) {
				break;
			}
			digger(nodelist.item(i), bean);
		}
	}

	public double getKvalue() {
		return kvalue;
	}

	public void setKvalue(double kvalue) {
		this.kvalue = kvalue;
	}

	public double getDvalue() {
		return dvalue;
	}

	public void setDvalue(double dvalue) {
		this.dvalue = dvalue;
	}

	public Date getDataDate() {
		return dataDate;
	}

	public void setDataDate(Date dataDate) {
		this.dataDate = dataDate;
	}

}
