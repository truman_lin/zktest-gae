package truman.testbed.KIndexing.model;

import java.util.List;
import java.util.Map;

public class Stocks {
    private List<String> stockList;
    private String twseUrl;
    private Map<String, Float> kmap;
    private Map<String, Float> dmap;
    private String kdate;

    public List<String> getStockList() {
	return stockList;
    }

    public void setStockList(List<String> stockList) {
	this.stockList = stockList;
    }

    /**
     * @return the twseUrl
     */
    public String getTwseUrl() {
	return twseUrl;
    }

    /**
     * @param twseUrl
     *            the twseUrl to set
     */
    public void setTwseUrl(String twseUrl) {
	this.twseUrl = twseUrl;
    }

    /**
     * @return the kdate
     */
    public String getKdate() {
	return kdate;
    }

    /**
     * @param kdate
     *            the kdate to set
     */
    public void setKdate(String kdate) {
	this.kdate = kdate;
    }

    /**
     * @return the kmap
     */
    public Map<String, Float> getKmap() {
	return kmap;
    }

    /**
     * @param kmap
     *            the kmap to set
     */
    public void setKmap(Map<String, Float> kmap) {
	this.kmap = kmap;
    }

    /**
     * @return the dmap
     */
    public Map<String, Float> getDmap() {
	return dmap;
    }

    /**
     * @param dmap
     *            the dmap to set
     */
    public void setDmap(Map<String, Float> dmap) {
	this.dmap = dmap;
    }
}
