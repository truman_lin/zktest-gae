/**
 * 程式資訊摘要：
 * 類別名稱：.java
 * 程式內容說明：
 * 版本資訊：
 * 程式設計人員姓名：
 * 程式修改記錄：20xx-xx-xx
 * 版權宣告：
 */
package truman.testbed.KIndexing.model;

/**
 * @author user
 * 
 */
public class KDBean {
    private String date;
    private float rsv;
    private float close;
    private float high;
    private float low;
    private float kvalue;
    private float dvalue;

    /**
     * @return the date
     */
    public String getDate() {
	return date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(String date) {
	this.date = date;
    }

    /**
     * @return the rsv
     */
    public float getRsv() {
	return rsv;
    }

    /**
     * @param rsv
     *            the rsv to set
     */
    public void setRsv(float rsv) {
	this.rsv = rsv;
    }

    /**
     * @return the close
     */
    public float getClose() {
	return close;
    }

    /**
     * @param close
     *            the close to set
     */
    public void setClose(float close) {
	this.close = close;
    }

    /**
     * @return the high
     */
    public float getHigh() {
	return high;
    }

    /**
     * @param high
     *            the high to set
     */
    public void setHigh(float high) {
	this.high = high;
    }

    /**
     * @return the low
     */
    public float getLow() {
	return low;
    }

    /**
     * @param low
     *            the low to set
     */
    public void setLow(float low) {
	this.low = low;
    }

    /**
     * @return the kvalue
     */
    public float getKvalue() {
	return kvalue;
    }

    /**
     * @param kvalue
     *            the kvalue to set
     */
    public void setKvalue(float kvalue) {
	this.kvalue = kvalue;
    }

    /**
     * @return the dvalue
     */
    public float getDvalue() {
	return dvalue;
    }

    /**
     * @param dvalue
     *            the dvalue to set
     */
    public void setDvalue(float dvalue) {
	this.dvalue = dvalue;
    }
}
