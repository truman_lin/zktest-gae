package truman.testbed;

import java.io.Serializable;

public class CheckBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3509764373799703545L;
	private String code;
	private String name;
	private boolean chked;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isChked() {
		return chked;
	}
	public void setChked(boolean chked) {
		this.chked = chked;
	}
}
