package truman.testbed;

import java.io.Serializable;
import java.util.Date;

public class Item implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1506238381105907861L;
	private int id;
	private boolean sel;
	private String name;
	private Date createDate;
	private String icon;


	public Item(int id, String name, String icon, Date createDate) {
		super();
		this.id = id;
		this.name = name;
		this.createDate = createDate;
		this.icon = icon;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Date getCreateDate() {
		return createDate;
	}
	
	public String getIcon() {
		return icon;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public boolean isSel() {
		return sel;
	}

	public void setSel(boolean sel) {
		this.sel = sel;
	}
}
