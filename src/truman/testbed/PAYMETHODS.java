package truman.testbed;

public enum PAYMETHODS {
	METHOD1("1","Cash"),METHOD2("2","Check"),METHOD3("3","CreditCard");
	private String code;
	private String method;
	private PAYMETHODS(String code, String method){
		this.code = code;
		this.method = method;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
}
