package truman.testbed;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;

public class LetterEVM {
	private Component innerPage;
	private boolean btnIndex = true;

	@Init
	public void init(){
		System.out.println("initializing VM");
	}
	@Command
	@NotifyChange("btnIndex")
	public void doQuery(@BindingParam("area") Component area){
		btnIndex = false; 
		innerPage = Executions.createComponents("content.zul", area, null);
	}
	@Command
	@NotifyChange("btnIndex")
	public void queryList(){
		btnIndex = false; 
	}
	@Command
	@NotifyChange("btnIndex")
	public void doClean(){
		btnIndex = true; 
		if(innerPage!=null){
			innerPage.detach();
		}
	}
	public boolean isBtnIndex() {
		return btnIndex;
	}
	public void setBtnIndex(boolean btnIndex) {
		this.btnIndex = btnIndex;
	}

}
